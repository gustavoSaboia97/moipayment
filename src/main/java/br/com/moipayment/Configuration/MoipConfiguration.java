package br.com.moipayment.Configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MoipConfiguration {

    @Getter
    private static String token;
    @Getter
    private static String key;

    @Value("${moip.token}")
    public void setToken(String pToken){

        token = pToken;
    }

    @Value("${moip.key}")
    public void setKey(String pKey){

        key = pKey;
    }
}
