package br.com.moipayment.services;

import br.com.moip.Moip;
import br.com.moip.exception.UnauthorizedException;
import br.com.moip.exception.ValidationException;
import br.com.moip.models.Setup;
import br.com.moip.auth.Authentication;
import br.com.moip.auth.BasicAuth;

import br.com.moipayment.Configuration.MoipConfiguration;
import lombok.extern.slf4j.Slf4j;

import static br.com.moip.models.Setup.Environment.SANDBOX;

import java.util.Map;

@Slf4j
public class CustomerService {

    private MoipConfiguration moipConfiguration = new MoipConfiguration();

    public void createMoipCustomer(Map<String, Object> customer){

        Authentication auth = new BasicAuth(moipConfiguration.getToken(), moipConfiguration.getKey());

        try{

            Setup setup = new Setup().setAuthentication(auth).setEnvironment(SANDBOX);
            Map<String, Object> newCustomer = Moip.API.customers().create(customer, setup);

            log.info("Customer created on Wirecard/Moip Service");
        }catch (ValidationException ex){

            log.error("Not Valid Data Insertion");
        }catch (UnauthorizedException ex){

            log.error("UnauthorizedException: Invalid Token or Key");
        }
    }
}
