package br.com.moipayment.controllers;

import br.com.moipayment.model.customer.Customer;
import br.com.moipayment.model.phone.Phone;
import br.com.moipayment.model.shipping_address.ShippingAddress;
import br.com.moipayment.model.tax_document.TaxDocument;
import br.com.moipayment.model.wirecard.CustomerMoipStructure;
import br.com.moipayment.services.CustomerService;
import org.joda.time.DateTime;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CustomerController {

    @PostMapping("/customer")
    public void createCustomer(){

        CustomerService customerService = new CustomerService();

        Customer customer = new Customer();
        customer.setFullName("Gustavo Saboia");
        customer.setEmail("gustavosaboia97@hotmail.com");
        customer.setBirthDate(new DateTime("1997-06-05"));
        customer.setOwnId("gustavosaboia97@hotmail.com");

        TaxDocument taxDocument = new TaxDocument();
        taxDocument.setNumber("13783180724");
        taxDocument.setType("CPF");

        Phone phone = new Phone();
        phone.setAreaCode("22");
        phone.setCountryCode("55");
        phone.setNumber("992318796");

        ShippingAddress shippingAddress = new ShippingAddress();
        shippingAddress.setCity("Rio de Janeiro");
        shippingAddress.setCountry("Brasil");
        shippingAddress.setDistrict("Humaita");
        shippingAddress.setState("Rio de Janeiro");
        shippingAddress.setStreet("Rua Humaita");
        shippingAddress.setStreetNumber("66");
        shippingAddress.setZipCode("22261001");

        customer.setPhone(phone);
        customer.setTaxDocument(taxDocument);
        customer.setShippingAdress(shippingAddress);

        CustomerMoipStructure customerMoipStructure = new CustomerMoipStructure();

        Map<String, Object> customerMap = customerMoipStructure.getCustomerMap(customer);
        customerService.createMoipCustomer(customerMap);
    }
}
