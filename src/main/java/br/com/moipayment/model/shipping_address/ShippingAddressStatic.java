package br.com.moipayment.model.shipping_address;

public class ShippingAddressStatic {
    public static final String CITY = "city";
    public static final String DISTRICT = "district";
    public static final String STREET = "street";
    public static final String STREET_NUMBER = "streetNumber";
    public static final String STATE = "state";
    public static final String COUNTRY = "country";
    public static final String ZIP_CODE = "zipCode";
}