package br.com.moipayment.model.shipping_address;

import lombok.Data;

@Data
public class ShippingAddress {
    private String city;
    private String district;
    private String street;
    private String streetNumber;
    private String state;
    private String country;
    private String zipCode;
}

/*

Map<String, Object> shippingAddress = payloadFactory(
    value("city", "Sao Paulo"),
    value("district", "Itaim BiBi"),
    value("street", "Av. Brigadeiro Faria Lima"),
    value("streetNumber", "3064"),
    value("state", "SP"),
    value("country", "BRA"),
    value("zipCode", "01451001")
);

*/