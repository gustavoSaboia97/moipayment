package br.com.moipayment.model.wirecard;

import br.com.moipayment.model.customer.Customer;
import br.com.moipayment.model.phone.Phone;
import br.com.moipayment.model.shipping_address.ShippingAddress;
import br.com.moipayment.model.tax_document.TaxDocument;

import java.util.HashMap;
import java.util.Map;

import static br.com.moipayment.model.customer.CustomerStatic.*;
import static br.com.moipayment.model.shipping_address.ShippingAddressStatic.*;
import static br.com.moipayment.model.tax_document.TaxDocumentStatic.*;
import static br.com.moipayment.model.phone.PhoneStatic.*;

public class CustomerMoipStructure {

    public Map<String, Object> getCustomerMap(Customer customerObject){

        Map<String, Object> taxDocument = getTaxDocumentMap(customerObject.getTaxDocument());

        Map<String, Object> phone = getPhoneMap(customerObject.getPhone());

        Map<String, Object> shippingAddress = getShippingAddressMap(customerObject.getShippingAdress());

        return buildAndReturnCustomerMap(customerObject,taxDocument,phone,shippingAddress);
    }

    public Map<String, Object> getTaxDocumentMap(TaxDocument taxDocumentObject){

        Map<String, Object> taxDocument = new HashMap<>();

        taxDocument.put(TYPE,taxDocumentObject.getType());
        taxDocument.put(NUMBER,taxDocumentObject.getNumber());

        return taxDocument;
    }

    public Map<String, Object> getPhoneMap(Phone phoneObject){

        Map<String, Object> phone = new HashMap<>();

        phone.put(COUNTRY_CODE,phoneObject.getCountryCode());
        phone.put(AREA_CODE,phoneObject.getAreaCode());
        phone.put(PHONE_NUMBER,phoneObject.getNumber());

        return phone;
    }

    public Map<String, Object> getShippingAddressMap(ShippingAddress shippingAddressObject){

        Map<String, Object> shippingAddress = new HashMap<>();

        shippingAddress.put(CITY,shippingAddressObject.getCity());
        shippingAddress.put(DISTRICT,shippingAddressObject.getDistrict());
        shippingAddress.put(STREET,shippingAddressObject.getStreet());
        shippingAddress.put(STREET_NUMBER,shippingAddressObject.getStreetNumber());
        shippingAddress.put(STATE,shippingAddressObject.getState());
        shippingAddress.put(COUNTRY,shippingAddressObject.getCountry());
        shippingAddress.put(ZIP_CODE,shippingAddressObject.getZipCode());

        return shippingAddress;
    }

    public Map<String, Object> buildAndReturnCustomerMap(Customer customerObject, Map<String, Object> taxDocument, Map<String, Object> phone, Map<String, Object> shippingAddress){

        Map<String, Object> customer = new HashMap<>();

        customer.put(OWN_ID,customerObject.getOwnId());
        customer.put(FULLNAME,customerObject.getFullName());
        customer.put(EMAIL,customerObject.getEmail());
        customer.put(BIRTH_DATE,customerObject.getBirthDate());

        customer.put(TAX_DOCUMENT,taxDocument);
        customer.put(PHONE,phone);
        customer.put(SHIPPING_ADRESS,shippingAddress);

        return customer;
    }
}
//    Map<String, Object> newCustomer = Moip.API.customers().create(customer, setup);

/**
 * Map<String, Object> taxDocument = payloadFactory(
 *     value("type", "CPF"),
 *     value("number", "10013390023")
 * );
 *
 * Map<String, Object> phone = payloadFactory(
 *     value("countryCode", "55"),
 *     value("areaCode", "11"),
 *     value("number", "22226842")
 * );
 *
 * Map<String, Object> shippingAddress = payloadFactory(
 *     value("city", "Sao Paulo"),
 *     value("district", "Itaim BiBi"),
 *     value("street", "Av. Brigadeiro Faria Lima"),
 *     value("streetNumber", "3064"),
 *     value("state", "SP"),
 *     value("country", "BRA"),
 *     value("zipCode", "01451001")
 * );
 *
 * Map<String, Object> taxDocumentHolder = payloadFactory(
 *     value("type", "CPF"),
 *     value("number", "22288866644")
 * );
 *
 * Map<String, Object> phoneHolder = payloadFactory(
 *     value("countryCode", "55"),
 *     value("areaCode", "11"),
 *     value("number", "55552266")
 * );
 *
 * Map<String, Object> holder = payloadFactory(
 *     value("fullname", "Test Holder Moip"),
 *     value("birthdate", "1990-10-22"),
 *     value("taxDocument", taxDocumentHolder),
 *     value("phone", phoneHolder)
 * );
 *
 * Map<String, Object> creditCard = payloadFactory(
 *     value("expirationMonth", "05"),
 *     value("expirationYear", "22"),
 *     value("number", "4012001037141112"),
 *     value("cvc", "123"),
 *     value("holder", holder)
 * );
 *
 * Map<String, Object> fundingInstrument = payloadFactory(
 *     value("method", "CREDIT_CARD"),
 *     value("creditCard", creditCard)
 * );
 *
 * Map<String, Object> customer = payloadFactory(
 *     value("ownId", "customer_own_id"),
 *     value("fullname", "Customer Full Name"),
 *     value("email", "customer@mail.com"),
 *     value("birthDate", "1980-5-10"),
 *     value("taxDocument", taxDocument),
 *     value("phone", phone),
 *     value("shippingAddress", shippingAddress),
 *     value("fundingInstrument", fundingInstrument)
 * );
 *
 * Map<String, Object> newCustomer = Moip.API.customers().create(customer, setup);
 * */