package br.com.moipayment.model.phone;

import lombok.Data;

@Data
public class Phone {
    private String countryCode;
    private String areaCode;
    private String number;
}