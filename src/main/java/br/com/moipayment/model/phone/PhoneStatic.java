package br.com.moipayment.model.phone;

public class PhoneStatic {
    public static final String COUNTRY_CODE = "countryCode";
    public static final String AREA_CODE = "areaCode";
    public static final String PHONE_NUMBER = "number";
}
