package br.com.moipayment.model.customer;

import br.com.moipayment.model.phone.Phone;
import br.com.moipayment.model.shipping_address.ShippingAddress;
import br.com.moipayment.model.tax_document.TaxDocument;

import lombok.Data;
import org.joda.time.DateTime;

@Data
public class Customer {

    private String ownId;
    private String fullName;
    private String email;
    private DateTime birthDate;
    private TaxDocument taxDocument;
    private Phone phone;
    private ShippingAddress shippingAdress;
}
