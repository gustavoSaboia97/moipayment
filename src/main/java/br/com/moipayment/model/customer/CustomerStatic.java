package br.com.moipayment.model.customer;

public class CustomerStatic {
    public static final String OWN_ID = "ownId";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String BIRTH_DATE = "birthDate";
    public static final String TAX_DOCUMENT = "taxDocument";
    public static final String PHONE = "phone";
    public static final String SHIPPING_ADRESS = "shippingAddress";
}