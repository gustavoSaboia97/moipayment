package br.com.moipayment.model.tax_document;

import lombok.Data;

@Data
public class TaxDocument {
    private String type;
    private String number;
}
