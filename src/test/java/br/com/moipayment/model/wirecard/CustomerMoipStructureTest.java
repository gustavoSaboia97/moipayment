package br.com.moipayment.model.wirecard;

import br.com.moipayment.model.customer.Customer;
import br.com.moipayment.model.phone.Phone;
import br.com.moipayment.model.shipping_address.ShippingAddress;
import br.com.moipayment.model.tax_document.TaxDocument;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Map;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.when;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomerMoipStructureTest {

    @Mock
    private Customer customerMock;
    @Mock
    private TaxDocument taxDocumentMock;
    @Mock
    private Phone phoneMock;
    @Mock
    private ShippingAddress shippingAddressMock;

    @Mock
    private Map<String, Object> taxDocumentMapMock;
    @Mock
    private Map<String, Object> phoneMapMock;
    @Mock
    private Map<String, Object> shippingAddressMapMock;
    @Mock
    private Map<String, Object> customerMapMock;

    private CustomerMoipStructure customerMoipStructure;

    @Before
    public void setup(){

        initMocks(this);

        customerMoipStructure = spy(new CustomerMoipStructure());
    }

    @Test
    public void shouldSetACustomerAndReturnCustomerMap(){

        when(customerMock.getTaxDocument()).thenReturn(taxDocumentMock);
        when(customerMock.getPhone()).thenReturn(phoneMock);
        when(customerMock.getShippingAdress()).thenReturn(shippingAddressMock);

        doReturn(taxDocumentMapMock).when(customerMoipStructure).getTaxDocumentMap(taxDocumentMock);
        doReturn(phoneMapMock).when(customerMoipStructure).getPhoneMap(phoneMock);
        doReturn(shippingAddressMapMock).when(customerMoipStructure).getShippingAddressMap(shippingAddressMock);
        doReturn(customerMapMock).when(customerMoipStructure).buildAndReturnCustomerMap(customerMock,taxDocumentMapMock,phoneMapMock,shippingAddressMapMock);

        Map<String, Object> response = customerMoipStructure.getCustomerMap(customerMock);

        verify(customerMoipStructure).getTaxDocumentMap(taxDocumentMock);
        verify(customerMoipStructure).getPhoneMap(phoneMock);
        verify(customerMoipStructure).getShippingAddressMap(shippingAddressMock);
        verify(customerMoipStructure).buildAndReturnCustomerMap(customerMock,taxDocumentMapMock,phoneMapMock,shippingAddressMapMock);

        assertThat(response).isEqualTo(customerMapMock);
    }
}
