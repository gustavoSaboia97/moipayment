package br.com.moipayment.controller;

import br.com.moipayment.controllers.CustomerController;
import br.com.moipayment.model.customer.Customer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.MockitoAnnotations.initMocks;

public class CustomerControllerTest {

    private CustomerController customerController;

    @Mock
    private Customer data;

    @Before
    public void setup(){

        initMocks(this);

        this.customerController = new CustomerController();
    }

    @Test
    public void shoudCreateACustomer(){

        customerController.createCustomer();
    }
}
